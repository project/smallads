<?php

namespace Drupal\smallads_mcapi\Plugin\Field;

use \Drupal\mcapi\Entity\Wallet;
use Drupal\mcapi\Entity\Storage\WalletStorage;
use Drupal\Core\TypedData\ComputedItemListTrait;
use Drupal\Core\Field\EntityReferenceFieldItemList;

/**
 * Computed value plugin for smallad wallet field.
 *
 * Compute the value of the wallet of the entity's owner.
 */
class SmalladWallet extends EntityReferenceFieldItemList {

  use ComputedItemListTrait;

  public function computeValue() {
    $owner_id = $this->getEntity()->getOwner()->id();
    foreach (WalletStorage::allWalletIdsOf($owner_id) as $delta => $wid) {
      $this->list[0] = $this->createItem($delta, Wallet::load($wid));
    }
  }
}

