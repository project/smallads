<?php

namespace Drupal\smallads_mcapi;

use Drupal\smallads\Entity\Smallad;
use Drupal\mcapi\Entity\Storage\WalletStorage;

/**
 * Compatibility with mutual_credit package
 */
class FirstWallet {

  /**
   * DefaultValueCallback for wallet field.
   *
   * Get the first wallet of the owner of the given smallad.
   *
   * @param Smallad $entity
   *
   * @return array
   *   The wallet ID
   */
  static function get(Smallad $entity) {
    $wids = WalletStorage::allWalletIdsOf($entity->getOwnerId());
    // N.B. result MUST be an array
    return array_slice($wids, 0, 1);
  }

}
