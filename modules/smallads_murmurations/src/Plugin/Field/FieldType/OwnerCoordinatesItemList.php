<?php

namespace Drupal\smallads_murmurations\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;
use Drupal\Core\TypedData\TraversableTypedDataInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Represents a configurable entity path field.
 */
class OwnerCoordinatesItemList extends FieldItemList {

  use ComputedItemListTrait;

  /**
   * Coordinates
   * @var array
   */
  protected $fallback;

  /**
   * Constructor
   * @param $definition
   * @param $name
   * @param $parent
   * @param ConfigFactoryInterface $config_factory
   */
  public function __construct($definition, $name, $parent, ConfigFactoryInterface $config_factory) {
    parent::__construct($definition, $name, $parent);
    $point = $config_factory->get('smallads_murmurations.settings')->get('fallback_point');
    if ($point != 'POINT ()') {
      $this->fallback = $point;
    }
  }

  /**
   * @param array $definition
   * @param $name
   * @param TraversableTypedDataInterface $parent
   * @return static
   */
  public static function createInstance($definition, $name = NULL, TraversableTypedDataInterface $parent = NULL) {
    return new static(
      $definition,
      $name,
      $parent,
      \Drupal::getContainer()->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   *
   * When rendering many smallads with the same owner in one page request,
   * increase the displacement each time
   */
  protected function computeValue() {
    static $previous_requests = [];
    $fieldname = smallads_user_has_geofield();
    $owner = $this->getEntity()->getOwner();
    if ($value = $owner->{$fieldname}->value) {
      // Vary the location by this multiple of the count
      //$counted = array_count_values($previous_requests);
      //$scale = 3 + $counted[$owner->id()]??0;
      //$previous_requests = $owner->id();
      //$this->displace($value, $scale);
      $this->list[0] = $this->createItem(0, $value);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function defaultAccess($operation = 'view', AccountInterface $account = NULL) {
    return AccessResult::forbiddenIf(!$this->fallback);
  }

  /**
   * {@inheritdoc}
   */
  protected function ensureComputedValue() {
    if ($this->valueComputed === FALSE and $this->fallback) {
      $this->computeValue();
    }
    $this->valueComputed = TRUE;
  }

}
