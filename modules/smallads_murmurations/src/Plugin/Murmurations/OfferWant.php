<?php

namespace Drupal\smallads_murmurations\Plugin\Murmurations;

use Drupal\smallads\Entity\SmalladInterface;
use Drupal\murmurations\Attribute\Murmurations as MurmsAttr;
use Drupal\murmurations\Plugin\Murmurations\PluginMultipleBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Shares the site data as murmurations offer/want schema.
 */
#[MurmsAttr(
  id: 'offer_want',
  label: new TranslatableMarkup('Offer/Want'),
  schema: 'offers_wants_schema-v0.1.0',
  profile_path: 'ad/{smallad}/murmurations.json',
  config: 'murmurations.currency_profile',
  default_aggregator: 'https://murmagg.communityforge.net/',
  entity_type: 'smallad'
)]
class OfferWant extends PluginMultipleBase {

  function getProfile() : array {
    /** @var \Drupal\smallads\Entity\Smallad $smallad */
    $smallad = $this->entity;
    // buy-sell, borrow-lend, rent-lease, receive-donate
    $t_types = [];
    if ($smallad->money->value) {
      $t_types[] = 'buy-sell';
    }
    if ($smallad->free->value) {
      $t_types[] = 'receive-donate';
    }
    if ($smallad->service->value) {
      $t_types[] = $smallad->money->value ? 'rent-lease' : 'borrow-lend';
    }
    if (empty($t_types)) {
      $t_types[] = 'buy-sell';
    }
    $tags = array_map(
      function ($term) {return $term->label();},
      $smallad->categories->referencedEntities()
    );

    $output = parent::getProfile() + [
      'description' => trim(strip_tags($smallad->body->value)),
      'exchange_type' => $smallad->bundle(),
      'item_type' => $smallad->service->value ? 'service' : 'good',
      'transaction_type' => $t_types,
      'geographic_scope' => 'local', // This is not the visibility scope, which is about privacy. Smallads doesn't have a field for this.
      'tags' => $tags,
      'contact_details' => [
        'contact_form' => \Drupal::urlGenerator()->generateFromRoute('entity.user.contact_form', ['user' => $smallad->getOwnerId()], ['absolute' => TRUE]),
      ]
    ];
    if ($expires = $smallad->expires->value) {
      $output['expires_at'] = strtotime($expires);
    }
    if (property_exists($smallad, 'external_link')) {
      if ($details = $smallad->external_link->value) {
        $output['details_url'] = $details;
      }
    }
    if (isset($smallad->image) and !$smallad->image->isEmpty()) {
      $output['image'] = $smallad->image->entity->createFileUrl(FALSE);
    }
    return $output;
  }

  /**
   * {@inheritDoc}
   */
  function needed() : string {
    return smallads_murmurations_user_has_geofield() ? '' : t('The user entity must have a geofield.');
  }

  /**
   * Get the ids of the entities to share.
   *
   * @return array
   */
  function getEntityIds() : array {
    return \Drupal::entityQuery('smallad')->accessCheck(TRUE)
      ->condition('scope', SmalladInterface::SCOPE_NETWORK, '>=')
      ->condition('type', ['offer', 'want'], 'IN')
      ->execute();
  }

  /**
   * {@inheritDoc}
   */
  function publishable() : bool {
    return $this->entity->scope->value >= SmalladInterface::SCOPE_NETWORK;
  }

  /**
   * {@inheritDoc}
   */
  function filterFormAlter(array &$form, FormStateInterface $form_state, array $defaults) {
    $defaults = array_filter($defaults);
    $form['#attached']['library'][] = 'smallads_murmurations/filter_form'; //flatten radio buttons
    $defaults += ['transaction_type' => '', 'item_type' => '', 'exchange_type' => []];
    $form['item_type'] = [
      '#title' => $this->t('Item type'),
      '#type' => 'radios',
      '#options' => $this->itemTypes() + ['' => $this->t('Either')],
      '#default_value' => $defaults['item_type'],
      '#weight' => 3
    ];
    $form['transaction_type'] = [
      '#title' => $this->t('Transaction types'),
      '#description' => $this->t('Leave empty to choose any type'),
      '#type' => 'checkboxes',
      '#options' => $this->transactionTypes(),
      '#default_value' => explode(',',$defaults['transaction_type']),
      '#weight' => 4
    ];
    if (!$this->getExchangeType()) {
      $form['exchange_type'] = [
        '#title' => $this->t('Exchange types'),
        '#type' => 'radios',
        '#options' => $this->exchangeTypes(),
        '#default_value' => $defaults['exchange_type'],
        '#required' => TRUE,
        '#weight' => 4
      ];
    }
  }

  /**
   * {@inheritDoc}
   */
  function filterFormValues(array $values) : array {
    $filters = [];
    $filters['exchange_type'] = $values['exchange_type']??$this->getExchangeType();
    if (!empty($values['item_type'])) {
      $filters['item_type'] = $values['item_type'];
    }
    if (!empty($values['transaction_type'])) {
      $ttypes = array_keys(array_filter($values['transaction_type']));
      $filters['transaction_type'] = implode(',', $ttypes);
    }
    return $filters;
  }

  private function getExchangeType() : string {
    $options = \Drupal::request()->attributes->get('_route_object')->getOptions();
    return $options['exchange_type']??'';
  }

  /**
   * {@inheritDoc}
   */
  function renderResult(\stdClass $result) : \Drupal\Core\Render\Markup {
    $transaction_types = explode(',', $result->transaction_type);
    $markup = $this->itemTypes($result->item_type) . '; '.implode(', ', $this->transactionTypes($transaction_types));
    $markup .= '<br />'.$result->description;
    return \Drupal\Core\Render\Markup::create($markup);
  }

  private function transactionTypes(array $types = NULL) : array {
    $exchange_type = $this->getExchangeType();
    if ($exchange_type == 'offer') {
      $vals = [
        'buy-sell' => t('Sell'),
        'receive-donate' => $this->t('Donate'),
        'rent-lease' => $this->t('Lease'),
        'borrow-lend' => $this->t('Lend'),
      ];
    }
    elseif ($exchange_type == 'want') {
      $vals = [
        'buy-sell' => t('Buy'),
        'receive-donate' => $this->t('Receive'),
        'rent-lease' => $this->t('Rent'),
        'borrow-lend' => $this->t('Borrow'),
      ];
    }
    else {
      $vals = [
        'buy-sell' => t('Buy/Sell'),
        'receive-donate' => $this->t('Receive/Donate'),
        'rent-lease' => $this->t('Rent/Lease'),
        'borrow-lend' => $this->t('Borrow/Lend'),
      ];
    }
    if ($types) {
      return array_intersect_key($vals, array_flip($types));
    }
    return $vals;
  }

  private function itemTypes(string $type = NULL) {
    $types = [
      'good' => $this->t('Good'),
      'service' => $this->t('Service'),
    ];
    if ($type) {
      return $types[$type];
    }
    return $types;
  }

  private function exchangeTypes(string $type = NULL) {
    $types = [
      'offer' => $this->t('Offer'),
      'want' => $this->t('Want'),
    ];
    if ($type) {
      return $types[$type];
    }
    return $types;
  }

}
