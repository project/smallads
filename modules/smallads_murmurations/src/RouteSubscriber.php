<?php

namespace Drupal\smallads_murmurations;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

/**
 * Add routes for global offers and wants, list and map.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($collection->get('view.smallads_auto_page.offer')) {
      $collection->add('smallads_murmurations.offer.map',
        new Route(
          '/offers/global',
          [
            '_form' => '\Drupal\murmurations\Form\FilterFormMap',
            '_title' =>  'Search network offers'
          ],
          ['_permission' => 'post smallad'],
          [
            'plugin' => 'offer_want',
            'exchange_type' => 'offer'
          ]
        )
      );
      $collection->add('smallads_murmurations.offer.list',
        new Route(
          '/offers/global/list',
          [
            '_form' => '\Drupal\murmurations\Form\FilterFormList',
            '_title' =>  'Search network offers'
          ],
          ['_permission' => 'post smallad'],
          [
            'plugin' => 'offer_want',
            'exchange_type' => 'offer'
          ]
        )
      );
    }
    if ($collection->get('view.smallads_auto_page.want')) {
      $collection->add('smallads_murmurations.want.map',
        new Route(
          '/wants/global',
          [
            '_form' => '\Drupal\murmurations\Form\FilterFormMap',
            '_title' =>  'Search network wants'
          ],
          [
            '_permission' => 'post smallad'
          ],
          [
            'plugin' => 'offer_want',
            'exchange_type' => 'want'
          ]
        )
      );
      $collection->add('smallads_murmurations.want.list',
        new Route(
          '/wants/global/list',
          [
            '_form' => '\Drupal\murmurations\Form\FilterFormList',
            '_title' =>  'Search network wants'
          ],
          [
            '_permission' => 'post smallad'
          ],
          [
            'plugin' => 'offer_want',
            'exchange_type' => 'want'
          ]
        )
      );
    }
    $collection->get('entity.user.contact_form')->setRequirements([
      '_access_contact_murms_offer_want' => 'TRUE',
      'user' => '\d+'
    ]);
  }

}
