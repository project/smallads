<?php

namespace Drupal\smallads_murmurations;

use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides local actions to create smallads of each type.
 */
class NetworkSearchMenuTasks extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;

  protected $storage = [];

  /**
   * {@inheritdoc}
   */
  public function __construct($smallad_type_storage) {
    $this->storage = $smallad_type_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('entity_type.manager')->getStorage('smallad_type')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    foreach (['offer', 'want'] as $type) {
      // Route names generated at smallads_smallad_type_insert
      $this->derivatives["murmurations.$type.global"] = [//smallads_murmurations.autotasks:murmurations.want.global
        'title' => $this->t('Global'),
        'description' => $this->t('Search ads accross the network'),
        'route_name' => "smallads_murmurations.$type.map",
        'base_route' => "view.smallads_auto_page.$type",  //Search ads on @site_name
        'weight' => 1,
      ] + $base_plugin_definition;

      // 'Map' and 'List' subtabs
      // Base route is automatically populated from the parent.
      $this->derivatives["murmurations.$type.global_map"] = [
        'title' => $this->t('Map'),
        'description' => $this->t('Show results on a map'),
        'route_name' => "smallads_murmurations.$type.map",
        'parent_id' =>  "smallads_murmurations.autotasks:murmurations.$type.global",
      ] + $base_plugin_definition;
      $this->derivatives["murmurations.$type.global_list"] = [
        'title' => $this->t('List'),
        'description' => $this->t('Show results as a list'),
        'route_name' => "smallads_murmurations.$type.list",
        'parent_id' =>  "smallads_murmurations.autotasks:murmurations.$type.global",
        'weight' => 1,
      ] + $base_plugin_definition;
    }
    return $this->derivatives;
  }

}
