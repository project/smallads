<?php

namespace Drupal\smallads_murmurations;

use Drupal\Core\Cache\CacheableResponse;

/**
 * Generate a siteMap, including the homePage
 */
class XmlSitemapController {

  /**
   * @var Drupal\path_alias\AliasManager
   */
  private $pathAliasManager;

  /**
   * @var
   */
  private $sitemap;

  function __construct() {
    if (\Drupal::moduleHandler()->moduleExists('path_alias')) {
      $this->pathAliasManager = \Drupal::service('path_alias_manager');
    }
    $this->sitemap = xmlwriter_open_memory();
    $this->sitemap->setIndent(2);
    $this->sitemap->setIndentString(' ');
  }

  /**
   * Page callback
   */
  function sitemap() {
    $this->sitemap->startDocument('1.0', 'UTF-8');
    $this->sitemap->startElement('urlset');

    $this->sitemap->startElement('url');
    $this->sitemap->startElement('loc');
    $this->sitemap->text('https://'.\Drupal::request()->getHost() . base_path());
    $this->sitemap->endElement();
    $this->sitemap->endElement();


    $smids = \Drupal::entityQuery('smallad')->accessCheck(TRUE)->condition('scope', 4)->execute();
    foreach (array_chunk($smids, 50) as $chunk) {
      foreach (Smallad::loadMultiple($chunk) as $smallad) {
        $this->addOnePage($smallad);
      }
    }
    $this->sitemap->endElement();
    $this->sitemap->endDocument();

    $response = new CacheableResponse('', 200);
    $response->setContent($this->sitemap->outputMemory());
    $response->addCacheableDependency($cache_metadata);
    $response->headers->set('Content-type', 'text/xml; charset=utf-8');
    return $response;
  }

  private function addOnePage($entity) {
    $path = $entity->toUrl(NULL, NULL, ['absolute' => TRUE]);
    if (isset($this->pathAliasManager)) {
      $path = $this->pathAliasManager->getAliasByPath($path);
    }

    $this->sitemap->startElement('url');
    $this->sitemap->startElement('loc');
    $this->sitemap->text($path);
    $this->sitemap->endElement();
    $this->sitemap->startElement('changefreq');
    $this->sitemap->text('daily');
    $this->sitemap->endElement();
    $this->sitemap->endElement();
  }

}
