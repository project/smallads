<?php

namespace Drupal\smallads_group\Plugin\Group\Relation;

use Drupal\smallads\Entity\SmalladType;
use Drupal\group\Plugin\Group\Relation\GroupRelationTypeInterface;
use Drupal\Component\Plugin\Derivative\DeriverBase;

class GroupSmalladDeriver extends DeriverBase {

  public function getDerivativeDefinitions($base_plugin_definition) {
    assert($base_plugin_definition instanceof GroupRelationTypeInterface);
    $this->derivatives = [];

    foreach (SmalladType::loadMultiple() as $name => $ad_type) {
      $label = $ad_type->label();
      $this->derivatives[$name] = clone $base_plugin_definition;
      $this->derivatives[$name]->set('entity_bundle', $name);
      $this->derivatives[$name]->set('label', t('Group smallad (@type)', ['@type' => $label]));
      $this->derivatives[$name]->set('description', t('Adds %type content to groups both publicly and privately.', ['%type' => $label]));
    }

    return $this->derivatives;
  }

}
