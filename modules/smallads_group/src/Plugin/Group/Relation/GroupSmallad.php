<?php

namespace Drupal\smallads_group\Plugin\Group\Relation;

use Drupal\group\Plugin\Group\Relation\GroupRelationBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a content enabler for transactions.
 *
 * @GroupRelationType(
 *   id = "group_smallad",
 *   label = @Translation("Group Smallad"),
 *   description = @Translation("Adds smallads to groups both publically and privately."),
 *   entity_type_id = "smallad",
 *   deriver = "Drupal\smallads_group\Plugin\Group\Relation\GroupSmalladDeriver"
 * )
 */
class GroupSmallad extends GroupRelationBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $config = parent::defaultConfiguration();
    $config['entity_cardinality'] = 1;
    return $config;
  }

  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    // From Drupal\gnode\Plugin\Group\Relation\GroupNode
    $info = $this->t("This field has been disabled by the plugin to guarantee the functionality that's expected of it.");
    $form['entity_cardinality']['#disabled'] = TRUE;
    $form['entity_cardinality']['#description'] .= '<br /><em>' . $info . '</em>';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    $dependencies = parent::calculateDependencies();
    $dependencies['config'][] = 'smallads.type.' . $this->getRelationType()->getEntityBundle();
    return $dependencies;
  }

//
//  /**
//   * {@inheritdoc}
//   */
//  public function getGroupOperations(GroupInterface $group) {
//    $type = $this->getEntityBundle();
//    $operations = [];
//    if ($group->hasPermission('create-edit-delete own smallads', \Drupal::currentUser())) {
//      $operations["create-$type"] = [
//        'title' => $this->t('Post new @type', ['@type' => $this->getSmalladType()->label()]),
//        'url' => new Url(
//          'entity.smallad.add_form',
//          ['group' => $group->id(), 'smallad_type' => $this->getEntityBundle()]
//        ),
//        'weight' => 5,
//      ];
//    }
//    return $operations;
//  }
//  /**
//   * {@inheritdoc}
//   *
//   * @note this will return identical permissions for every smallad type
//   */
//  public function getPermissions() {
//    $permissions['create-edit-delete own smallads']['title'] = 'Post small ads';
//    $permissions['manage-smallads']['title'] = 'Manage small ads';
//    return $permissions;
//  }
//
//  /**
//   * {@inheritdoc}
//   */
//  public function createAccess(GroupInterface $group, AccountInterface $account) {
//    return GroupAccessResult::allowedIfHasGroupPermission($group, $account, 'create-edit-delete own smallads');
//  }
//
//  /**
//   * {@inheritdoc}
//   */
//  protected function viewAccess(GroupContentInterface $group_content, AccountInterface $account) {
//    return GroupAccessResult::allowedIfHasGroupPermission(
//      $group_content->getGroup(),
//      $account,
//      'view group_membership content'
//    );
//  }
//
//  /**
//   * {@inheritdoc}
//   */
//  protected function updateAccess(GroupContentInterface $group_content, AccountInterface $account) {
//    return $this->edit_delete_access($group_content, $account);
//  }
//
//  /**
//   * {@inheritdoc}
//   */
//  protected function deleteAccess(GroupContentInterface $group_content, AccountInterface $account) {
//    return $this->edit_delete_access($group_content, $account);
//  }
//
//
//
//
//  private function edit_delete_access(GroupContentInterface $group_content, AccountInterface $account) {
//    $group = $group_content->getGroup();
//
//    // Allow members to edit their own smallads.
//    if ($group_content->entity_id->entity->id() == $account->id()) {
//      return GroupAccessResult::allowedIfHasGroupPermission($group, $account, 'create-edit-delete own smallads');
//    }
//
//    return GroupAccessResult::allowedIfHasGroupPermission($group, $account, 'manage-smallads');
//  }
//
//  /**
//   * Retrieves the node type this plugin supports.
//   *
//   * @return \Drupal\node\NodeTypeInterface
//   *   The node type this plugin supports.
//   */
//  private function getSmalladType() {
//    return SmalladType::load($this->getEntityBundle());
//  }

}
