<?php

namespace Drupal\smallads\Entity;

use Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Provides routes for the smallad entity.
 */
class SmalladRouteProvider extends DefaultHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);
    $collection->get('entity.smallad.canonical')
      ->setDefault('_title_callback', '\Drupal\smallads\Controller::canonicalLabel');
    return $collection;
  }

}
