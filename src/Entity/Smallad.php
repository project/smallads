<?php

namespace Drupal\smallads\Entity;

use Drupal\user\EntityOwnerInterface;
use Drupal\user\UserInterface;
use Drupal\user\EntityOwnerTrait;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Defines the Smallad entity.
 *
 * @ContentEntityType(
 *   id = "smallad",
 *   label = @Translation("Smallad"),
 *   bundle_label = @Translation("Smallad type"),
 *   label_collection = @Translation("Ads"),
 *   handlers = {
 *     "storage" = "Drupal\smallads\SmalladStorage",
 *     "view_builder" = "Drupal\smallads\SmalladViewBuilder",
 *     "access" = "Drupal\smallads\SmalladAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\smallads\Form\SmalladEdit",
 *       "edit" = "Drupal\smallads\Form\SmalladEdit",
 *       "delete" = "Drupal\smallads\Form\SmalladDeleteConfirm",
 *     },
 *     "views_data" = "Drupal\smallads\SmalladViewsData",
 *     "route_provider" = {
 *       "html" = "Drupal\smallads\Entity\SmalladRouteProvider",
 *     },
 *   },
 *   admin_permission = "acccess administration pages",
 *   base_table = "smallad",
 *   data_table = "smallad_field_data",
 *   entity_keys = {
 *     "id" = "smid",
 *     "label" = "title",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *     "bundle" = "type",
 *     "uid" = "uid",
 *     "owner" = "uid"
 *   },
 *   bundle_entity_type = "smallad_type",
 *   field_ui_base_route = "entity.smallad_type.edit_form",
 *   translatable = TRUE,
 *   links = {
 *     "canonical" = "/ad/{smallad}",
 *     "add-form" = "/ad/add/{smallad_type}",
 *     "edit-form" = "/ad/{smallad}/edit",
 *     "delete-form" = "/ad/{smallad}/delete"
 *   }
 * )
 */
class Smallad extends ContentEntityBase implements SmalladInterface, EntityOwnerInterface, EntityChangedInterface, EntityPublishedInterface {

  use StringTranslationTrait;
  use EntityChangedTrait;
  use EntityOwnerTrait;


  /**
   * {@inheritDoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::ownerBaseFieldDefinitions($entity_type);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setDescription(t('One-line description'))
      ->setRequired(TRUE)
      ->setTranslatable(TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['body'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Tell the story behind this ad.'))
      ->setRequired(FALSE)
      ->setTranslatable(TRUE)
      // This could break in a migration
      ->setSettings(['allowed_formats' => [filter_fallback_format()]])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['uid'] //alterations to the ::ownerBaseFieldDefinition
      ->setLabel(t('Posted by'))
      ->setDescription(t('The owner of the small ad.'))
      ->setRevisionable(FALSE)
      ->setRequired(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ],
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the ad was created.'))
      ->setDisplayOptions('view', array(
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => 10,
      ))
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Last changed'))
      ->setDescription(t('When the ad was last saved.'))
      ->setRevisionable(FALSE)
      ->setTranslatable(FALSE)
      //->setInitialValue('created')
      ->setDisplayConfigurable('view', TRUE);

    $fields['scope'] = BaseFieldDefinition::create('smallad_scope')
      ->setLabel(t('Visibility'))
      ->setDescription(t('How widely this ad can be seen.'))
      ->setDisplayOptions('form', [
        'type' => 'options_buttons', // @note there is special theming for this: smallad_scope_widget
        'weight' => 6,
      ])
      ->setDisplayConfigurable('view', TRUE)
      // Something wrong with this but I can't find an example of how it should be done.
      //->addConstraint ('AllowedValues', ['choices' => array_keys(ScopeItem::validScopes())])
      ->setDefaultValueCallback('Drupal\smallads\Plugin\Field\FieldType\ScopeItem::smalladsDefaultScope')
      ->setRequired(TRUE);

    // This depends on the datetime module.
    $fields['expires'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Expiry date'))
      ->setDescription(t('If set, ad will revert to private on this date.'))
      ->setRevisionable(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 10,
      ])
      ->addConstraint ('smallad_is_visible', [])
      ->setSetting('datetime_type', 'date')
      ->setDisplayConfigurable('view', TRUE)
      ->setDefaultValueCallback('Drupal\smallads\Entity\Smallad::expiresDefault')
      ->setTranslatable(FALSE)
      ->setRevisionable(FALSE)
      ->setRequired(FALSE);
    
    $cat_card = \Drupal::config('smallads.settings')->get('categories_cardinality') ?? 3;
    $fields['categories'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Category'))
      ->setDescription(t('Categories (max 3)'))
      ->setRevisionable(FALSE)
      ->setSetting('target_type', 'taxonomy_term')
      ->setSetting('handler', 'default:taxonomy_term')
      ->setSetting('handler_settings', ['target_bundles' => ['categories']])
      ->setRequired(TRUE)
      ->setCardinality($cat_card) // 3 on installation, can be modified in settings.
      ->setTranslatable(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['free'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Free'))
      ->setDescription(t('No reciprocation required'))
      ->setRevisionable(FALSE)
      ->setTranslatable(FALSE)
      ->setDefaultValue(0)
      ->setCardinality(1)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
    $fields['service'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Temporary'))
      ->setDescription(t('This is a service, or item for temporary use'))
      ->setRevisionable(FALSE)
      ->setTranslatable(FALSE)
      ->setDefaultValue(0)
      ->setCardinality(1)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
    $fields['money'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Money'))
      ->setDescription(t('Some or all of the payment will be in money.'))
      ->setRevisionable(FALSE)
      ->setTranslatable(FALSE)
      ->setDefaultValue(0)
      ->setCardinality(1)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * {@inheritDoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritDoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritDoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritDoc}
   */

  public static function expiresDefault() {
    if ($interval = \Drupal::config('smallads.settings')->get('default_expiry')) {
      return DrupalDateTime::createFromTimestamp(strtotime($interval))->format('Y-m-d');
    }
  }

  /**
   * {@inheritDoc}
   */
  public function expire() {
    // There might be an option on notification somewhere?
    $this->scope->value = SmalladInterface::SCOPE_PRIVATE;
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function getCreatedTime() {
    if (isset($this->get('created')->value)) {
      return $this->get('created')->value;
    }
    return NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function isPublished(){
    return $this->scope->value > SmalladInterface::SCOPE_PRIVATE;
  }

  /**
   * {@inheritDoc}
   *
   * Not used in this module.
   */
  public function setPublished($published = NULL) {
    $this->scope->value = SmalladInterface::SCOPE_SITE;
  }

  /**
   * {@inheritDoc}
   */
  public function setUnPublished($published = NULL){
    return $this->expire();
  }

  /**
   * {@inheritDoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);
    smallads_reindex_smallad_search($this->id());
  }
}

