<?php

namespace Drupal\smallads;
use Drupal\smallads\Entity\SmalladInterface;

/**
 * Returns responses for Smallad routes.
 */
class Controller {

  /**
   * {@inheritdoc}
   */
  public function canonicalLabel(SmalladInterface $smallad) {
    return t(
      '@type: %title',
        [
          '@type' => $smallad->type->entity->label(),
          '%title' => $smallad->label(),
        ]
    );
  }

}
