<?php

namespace Drupal\smallads;

use Drupal\smallads\Entity\SmalladInterface;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Defines an access controller for the Smallad entity.
 */
class SmalladAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $smallad, $operation, AccountInterface $account) {
    if ($account->hasPermission('access administration pages')) {
      $result = AccessResult::allowed()->cachePerUser();
    }
    // Owners can do anything with their own content.
    elseif ($smallad->getOwnerId() == $account->id() or $account->hasPermission('edit all smallads')) {
      $result = AccessResult::allowed()->cachePerUser();
    }
    elseif ($operation == 'view') {
      // Note that smallads_views_query_alter() only support public, site and private access.
      switch ($smallad->scope->value) {
        case SmalladInterface::SCOPE_PUBLIC:
        case SmalladInterface::SCOPE_NETWORK:
          //This may be search indexed. Anyone can see it.
          $result = AccessResult::allowed();
          break;
        case SmalladInterface::SCOPE_SITE:
          //Only members of the site can see it.
          $result = AccessResult::allowedIf($account->isAuthenticated());
          break;
        case SmalladInterface::SCOPE_GROUP:
          $result = parent::checkAccess($smallad, $operation, $account);
          break;
          throw new \Exception('If groups are enabled SmalladAccessControlHandler should be overridden ');
        case SmalladInterface::SCOPE_PRIVATE:
          $result = AccessResult::forbidden('Small ad is private');
      }
    }
    else {
      $result = AccessResult::forbidden();
    }
    return $result->addCacheableDependency($smallad);
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'post smallad');
  }

  /**
   * {@inheritDoc}
   */
  protected function checkFieldAccess($operation, FieldDefinitionInterface $field_definition, AccountInterface $account, FieldItemListInterface $items = NULL) {
    if ($operation == 'edit' && $field_definition->getName() == 'uid') {
      return AccessResult::allowedIfHasPermission($account, 'edit all smallads');
    }
    return parent::checkFieldAccess($operation, $field_definition, $account, $items);
  }

}
