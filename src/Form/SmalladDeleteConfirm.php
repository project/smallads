<?php

namespace Drupal\smallads\Form;
use Drupal\Core\Url;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * The form to confirm deletion of an ad.
 */
class SmalladDeleteConfirm extends ContentEntityDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t("Ads can be deleted or simply not shown to other members by reducing their scope.");
  }

  /**
   * {@inheritdoc}
   */
  protected function getRedirectUrl() {
    $account = \Drupal::currentUser();
    if ($account->hasPermission('edit all smallads')) {
      $url = Url::fromRoute('view.smallads_admin.collection');
    }
    else {
      $url = Url::fromRoute('view.smallads_user.my_ads', ['user' => $account->id()]);
    }
    return $url;
  }

}
