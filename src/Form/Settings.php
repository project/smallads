<?php

namespace Drupal\smallads\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteBuilderInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form for configuring this module.
 */
class Settings extends ConfigFormBase {

  /**
   * @var RouteBuilderInterface
   */
  protected $routeBuilder;

  /**
   * @param ConfigFactoryInterface $config_factory
   * @param RouteBuilderInterface $router_builder
   */
  function __construct(\Drupal\Core\Config\ConfigFactoryInterface $config_factory, RouteBuilderInterface $router_builder) {
    parent::__construct($config_factory);
    $this->routeBuilder = $router_builder;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('router.builder')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'smallads_config_form';
  }

  /**
   * {@inheritDoc}
   *
   * @todo compose emails for expiry
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->configFactory->get('smallads.settings');
    $form['default_expiry'] = [
      '#title' => $this->t('Default expiry date on ads'),
      '#description' => $this->t('Default value of the small ad expiry form widget'),
      '#type' => 'select',
      '#options' => [
        '' => t('Never'),//the date widget can't handle empty''
        '+1 week' => $this->t('1 week'),
        '+1 month' => $this->t('1 month'),
        '+3 months' => $this->t('@count months', ['@count' => 3]),
        '+6 months' => $this->t('@count months', ['@count' => 6]),
        '+1 year' => $this->t('1 year'),
      ],
      '#default_value' => $config->get('default_expiry'),
    ];

    $form['categories_cardinality'] = [
      '#title' => $this->t('Maximum number of categories'),
      '#type' => 'number',
      '#min' => 1,
      '#max' => 5,
      '#default_value' => $config->get('categories_cardinality'),
      '#weight' => 2
    ];

    // Since there is a datatype for storing email templates I'm surprised there's no form widget
    $form['expiry_mail'] = [
      '#title' => $this->t('Expiry mail'),
      '#description' => $this->t('This mail is sent to the owner when an ad passes its expiry date.'),
      '#type' => 'details',
      '#open' => TRUE,
      '#tree' => TRUE,
      '#collapsible' => TRUE,
      'subject' => [
        '#title' => $this->t('Subject line for the expiry mail'),
        '#type' => 'textfield',
        '#default_value' => $config->get('expiry_mail')['subject'],
        '#weight'=> 0
      ],
      'body' => [
        '#title' => $this->t('Template of the expiry mail'),
        '#description' => $this->t('Use Smallads, User and Site tokens, e.g. [smallad:link], [user:edit-url] [site:name])'),
        '#type' => 'textarea',
        '#default_value' => $config->get('expiry_mail')['body'],
        '#weight'=> 1
      ]
    ];
    $form['comment_mail'] = [
      '#title' => $this->t('Comment mail'),
      '#description' => $this->t('This mail is sent to the owner when someone comments on their ad.'),
      '#type' => 'details',
      '#open' => TRUE,
      '#tree' => TRUE,
      '#collapsible' => TRUE,
      'subject' => [
        '#title' => $this->t('Subject line for the comment mail'),
        '#type' => 'textfield',
        '#default_value' => $config->get('comment_mail')['subject'],
        '#weight'=> 0
      ],
      'body' => [
        '#title' => $this->t('Template of the comment mail'),
        '#description' => $this->t('Use Smallads, Comment, User and Site tokens, e.g. [smallad:link], [comment:author:display-name], [user:edit-url] [site:name])'),
        '#type' => 'textarea',
        '#default_value' => $config->get('comment_mail')['body'],
        '#weight'=> 1
      ],
      'token_tree' => [
        '#theme' => 'token_tree_link',
        '#token_types' => ['user', 'comment', 'smallad', 'site'],
        '#weight' => 3
      ]
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable('smallads.settings');
    $scopes = $form_state->getValue('scopes');
    unset($scopes[0]);
    $config
      ->set('default_expiry', $form_state->getValue('default_expiry'))
      ->set('expiry_mail', $form_state->getValue('expiry_mail'))
      ->set('comment_mail', $form_state->getValue('comment_mail'))
      ->set('categories_cardinality', $form_state->getValue('categories_cardinality'))
      ->set('scopes', $scopes)
      ->save();

    parent::submitForm($form, $form_state);
    $this->routeBuilder->rebuild();
  }

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames() {
    return ['smallads.settings'];
  }

}
