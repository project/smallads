<?php

namespace Drupal\smallads\Form;

use Drupal\smallads\Entity\SmalladInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Component\Datetime\TimeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
/**
 * Form to create/edit a smallad entity.
 */
class SmalladEdit extends ContentEntityForm {

  /**
   * @var RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * @var ParameterBag
   */
  protected $params;

  /**
   * @var DateFormatter
   */
  protected $dateFormatter;

  /**
   * The configured interval for future expiry of the smallad.
   * @var string
   */
  protected $defaultExpiry;

  /**
   * Constructor
   */
  public function __construct($entity_repository, $entity_type_bundle_info, TimeInterface $time, ConfigFactoryInterface $config_factory, RouteMatchInterface $routeMatch, RequestStack $requestStack, DateFormatter $date_formatter) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->defaultExpiry = $config_factory->get('smallads.settings')->get('default_expiry');
    $this->routeMatch = $routeMatch;
    $this->params = $requestStack->getCurrentRequest()->query;
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('config.factory'),
      $container->get('current_route_match'),
      $container->get('request_stack'),
      $container->get('date.formatter')
    );
  }

  /**
   * Overrides Drupal\Core\Entity\ContentEntityForm::form().
   *
   * @todo set the min and max dates on $this->entity->expires - but how?
   */
  public function form(array $form, FormStateInterface $form_state) {
    if ($this->params->has('title')) {
      $this->entity->title->value = $this->params->get('title');
      // Can't remember why this was necessary.
      $form['#action'] = '/ad/add/'.$this->entity->bundle();
    }
    $form = parent::form($form, $form_state);

    if ($this->entity->isNew() && $this->routeMatch->getParameter('taxonomy_term')) {
      $form['type'] = [
        '#type' => 'value',
        '#value' => $this->routeMatch->getParameter('taxonomy_term'),
      ];
    }
    $expires_element = &$form['expires']['widget'][0]['value'];
    // set the default to the configued value.
    if ($this->entity->isNew() or $expires_element['#default_value'] < (new DrupalDateTime())) {
      if ($this->defaultExpiry) {
        $expires_element['#required'] = TRUE;
        $expires_element['#default_value'] = DrupalDateTime::createFromTimestamp(strtotime($this->defaultExpiry));
      }
      else {
        $expires_element['#default_value'] = NULL;
        $expires_element['#required'] = FALSE;
      }
    }
    // NOT POSSIBLE to prevent selecting a day before today. see validate
    $form['scope']['#weight'] = $form['expires']['#weight'] = 0.5;

    // Change the theming of the scope widget and ensure it goes immediately before expired field.
    // N.B. scope widget cannot be configured with other entity form fields.
    $form['scope']['#theme'] = 'smallad_scope_widget';

    $form['uid']['widget'][0]['target_id']['#access'] = $this->currentUser()->hasPermission('edit all smallads');
    $form['#attached']['library'][] = 'smallads/css';
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $smallad = $this->entity;
    $now = $this->time->getRequestTime();
    // Get the expiry time as the start of the set day.
    $scope_is_private = $smallad->scope->value == SmalladInterface::SCOPE_PRIVATE;
    $expires = $smallad->expires->value ? strtotime($smallad->expires->value):  strtotime('+1 week'); // given time or never.
    $ad_type = $smallad->type->entity->label();
    if (!$scope_is_private and $smallad->expires->value >= $now) {
      // Notify user of expiry date
      $this->messenger()->addStatus(
        $this->t(
          'Your @ad_type will expire in @interval.',
          [
            '@ad_type' => $ad_type,
            '@interval' => $this->dateFormatter->formatInterval($expires - $now, 2)
          ]
        )
      );
    }
    elseif ($scope_is_private and $expires < $now) {
      // Set the scope of the ad to the member only the ad because the date was set to before now.
      $this->messenger()->addStatus(
        t('This @ad_type is hidden.', ['@ad_type' => $ad_type]),
        'warning'
      );
    }
    if ($smallad->isNew()) {
      // Update the owner's default scope to the same scope as this
      \Drupal::service('user.data')->set(
        'smallads',
        $smallad->getOwnerId(),
        'defaultscope',
        $form_state->getValue('scope')[0]['value']
      );
    }
  }

  /**
   * {@inheritDoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    parent::save($form, $form_state);
    $form_state->setRedirect('entity.smallad.canonical', ['smallad' => $this->entity->id()]);
  }


}
