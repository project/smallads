<?php

namespace Drupal\smallads\Plugin\migrate;

use Drupal\migrate\Plugin\MigrationDeriverTrait;
use Drupal\migrate_drupal\FieldDiscoveryInterface;
use Drupal\migrate\Plugin\MigrationPluginManager;
use Drupal\Core\Database\DatabaseExceptionWrapper;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Component\Plugin\Derivative\DeriverBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Deriver for Drupal 7 smallad migrations based on smallad types.
 */
class D7SmalladDeriver extends DeriverBase implements ContainerDeriverInterface {
  use MigrationDeriverTrait;

  /**
   * The base plugin ID this derivative is for.
   *
   * @var string
   */
  protected $basePluginId;

  /**
   * The migration field discovery service.
   *
   * @var \Drupal\migrate_drupal\FieldDiscoveryInterface
   */
  protected $fieldDiscovery;

  /**
   *
   * @var MigrationPluginManager
   */
  protected $migrationPlugins;


  /**
   * D7SmalladDeriver constructor.
   *
   * @param string $base_plugin_id
   * @param FieldDiscoveryInterface $field_discovery
   * @param MigrationPluginManager $migration_plugin_manager
   */
  public function __construct($base_plugin_id, FieldDiscoveryInterface $field_discovery, MigrationPluginManager $migration_plugin_manager) {
    $this->basePluginId = $base_plugin_id;
    $this->fieldDiscovery = $field_discovery;
    $this->migrationPlugins = $migration_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    // Translations don't make sense unless we have content_translation.
    return new static(
      $base_plugin_id,
      $container->get('migrate_drupal.field_discovery'),
      $container->get('plugin.manager.migration')

    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $source_plugin = static::getSourcePlugin('d7_field_instance');
    $source_plugin->checkRequirements();

    try {
      foreach (['offer'  => 'Offer', 'want' => 'Want'] as $smallad_type_id => $label) {
        // Read all field instance definitions in the source database.
        foreach ($source_plugin as $row) {
          $values = $base_plugin_definition;
          $values['label'] = t('@label (@type)', [
            '@label' => $label,
            '@type' => $smallad_type_id,
          ]);

          $values['source']['node_type'] = 'proposition';
          $values['source']['want'] = intval($smallad_type_id == 'want'); //this is plugin config
          $values['destination']['default_bundle'] = $smallad_type_id;

          /** @var \Drupal\migrate\Plugin\MigrationInterface $migration */
          $migration = $this->migrationPlugins->createStubMigration($values);
          if ($row->getSourceProperty('bundle') == 'proposition') {
            $this->fieldDiscovery->addBundleFieldProcesses($migration, 'node', 'proposition');
            $this->derivatives[$smallad_type_id] = $migration->getPluginDefinition();
          }
        }
      }
    }
    catch (DatabaseExceptionWrapper $e) {
      // Once we begin iterating the source plugin it is possible that the
      // source tables will not exist. This can happen when the
      // MigrationPluginManager gathers up the migration definitions but we do
      // not actually have a Drupal 7 source database.
    }
    return $this->derivatives;
  }

}
