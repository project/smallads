<?php

namespace Drupal\smallads\Plugin\migrate\source\d7;

use Drupal\node\Plugin\migrate\source\d7\Node;

/**
 * Drupal 7 proposition nodes in source db.
 *
 * @MigrateSource(
 *   id = "d7_smallad",
 *   source_module = "offers_wants"
 * )
 */
class Smallad extends Node {

  /**
   * {@inheritDoc}
   */
  public function query() {
    $query = parent::query();
    $query->innerJoin('offers_wants', 'ow', 'ow.nid = n.nid');
    $query->condition('ow.want', $this->configuration['want']);
    $query->addField('ow', 'want');
    $query->addField('ow', 'end');
    return $query;
  }

  /**
   * {@inheritDoc}
   */
  public function fields() {
    return parent::fields() + [
      'end' => $this->t('Expiry date'),
      'want' => $this->t('Smallad bundle'),
    ];
  }

}
