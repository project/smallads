<?php

namespace Drupal\smallads\Plugin\views\field;
use Drupal\smallads\Plugin\Field\FieldType\ScopeItem;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\views\Attribute\ViewsField;

/**
 * Field handler for the smallad::scope.
 *
 * @ingroup views_field_handlers
 */
#[ViewsField('smallads_scope')]
class Scope extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $raw = $this->getValue($values);
    $label = ScopeItem::validScopes()[$raw];
    // Should be translated.
    return ['#markup' => '<span class = "smallad-scope-'. $raw .'">' . $label . '</span>'];
  }

}
