<?php

namespace Drupal\smallads\Plugin\Derivative;

use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides local actions to create smallads of each type.
 */
class SmalladActions extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;

  protected $derivatives = [];
  protected $storage = [];

  /**
   * {@inheritdoc}
   */
  public function __construct($smallad_type_storage) {
    $this->storage = $smallad_type_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('entity_type.manager')->getStorage('smallad_type')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    foreach ($this->storage->loadMultiple() as $smallad_type) {
      $bundle = $smallad_type->id();
      $this->derivatives[$bundle.'.add_form.action'] = [
        'title' => $this->t('Create @smalladtype', ['@smalladtype' => $smallad_type->label()]),
        'route_name' => 'entity.smallad.add_form',
        'route_parameters' => ['smallad_type' => $bundle],
        'appears_on' => [
          'view.smallads_admin.collection',
          "view.smallads_auto_page.$bundle",
        ],
      ];

    }
    return $this->derivatives;
  }

}
