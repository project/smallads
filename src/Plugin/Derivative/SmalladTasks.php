<?php

namespace Drupal\smallads\Plugin\Derivative;

use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides local actions to create smallads of each type.
 */
class SmalladTasks extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;

  protected $storage = [];
  protected $siteConfig;

  /**
   * {@inheritdoc}
   */
  public function __construct($smallad_type_storage, $site_settings) {
    $this->storage = $smallad_type_storage;
    $this->siteConfig = $site_settings;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('entity_type.manager')->getStorage('smallad_type'),
      $container->get('config.factory')->get('core.site')
    );
  }

  /**
   * {@inheritdoc}
   * Make a default tab for the main views
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    foreach ($this->storage->loadMultiple() as $smallad_type_id => $smallad_type) {
      // Route names generated at smallads_smallad_type_insert
      $this->derivatives["{$smallad_type_id}_local"] = [
        'title' => $this->t("Local", ['@smalladtypes' => $smallad_type->labelPlural()]),
        'description' => $this->t('Search ads on @site_name', ['@site_name' => $this->siteConfig->get('name')]),
        'base_route' => 'view.smallads_auto_page.'. $smallad_type_id,
        'route_name' => 'view.smallads_auto_page.'. $smallad_type_id,
        'weight' => 1,
      ] + $base_plugin_definition;
    }
    return $this->derivatives;
  }

}
