<?php

namespace Drupal\smallads\Plugin\QueueWorker;

use Drupal\smallads\Entity\Smallad;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Logger\LoggerChannel;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\Attribute\QueueWorker;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Mail\MailManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Mails the owner of a small ad before it expires.
 */
#[QueueWorker(
  id: 'smallads_expired_mail',
  title: new TranslatableMarkup('Smallad expiry notification'),
  cron: ['time' => 60]
)]
class ExpiredMail extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * @var LoggerChannel
   */
  protected $logger;

  /**
   *
   * @var MailManagerInterface
   */
  protected $mailManager;

  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerChannel $logger_channel, MailManagerInterface $mail_manager) {
    $this->configuration = $configuration;
    $this->pluginId = $plugin_id;
    $this->pluginDefinition = $plugin_definition;
    $this->logger = $logger_channel;
    $this->mailManager = $mail_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.channel.smallads'),
      $container->get('plugin.manager.mail')
    );
  }

  /**
   * {@inheritdoc}
   *
   * @todo make a settings form and variables for the text of the notifcation mail.
   */
  public function processItem($ad_id) {
    $ad = Smallad::load($ad_id);
    if ($ad->scope->value) { // Ensures it doesn't run twice.
      $ad->expire()->save();
      $owner = $ad->getOwner();

      $this->mailManager->mail(
        'smallads',
        'expired',
        $owner->getEmail(),
        $owner->getPreferredLangcode(),
        [
          'smallad' => $ad,
          'user' => $owner
        ]
      );
    }
  }

}
