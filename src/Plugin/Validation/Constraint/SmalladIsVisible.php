<?php

namespace Drupal\smallads\Plugin\Validation\Constraint;

use \Drupal\Core\Entity\Plugin\Validation\Constraint\CompositeConstraintBase;
use Drupal\Core\Validation\Attribute\Constraint;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Check that the smallad scope and expiry date are compatible.
 */
#[Constraint(
  id: 'smallad_is_visible',
  label: new TranslatableMarkup('Checks that the smallad is visible if the date is after now.')
)]
class SmalladIsVisible extends CompositeConstraintBase {

  public string $datelate = 'If the expiry date is after now, the scope must not be private';
  public string $dateEarly = 'If the expiry date is after now, the scope must not be private';

  /**
   * {@inheritDoc}
   */
  public function coversFields() {
    return ['expires'];
  }

}
