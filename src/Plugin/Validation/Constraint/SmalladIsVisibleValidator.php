<?php

namespace Drupal\smallads\Plugin\Validation\Constraint;

use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;

/**
 * Every entry must be between different wallets.
 */
class SmalladIsVisibleValidator extends ConstraintValidator {

  /**
   * {@inheritDoc}
   */
  public function validate($expires_items, Constraint $constraint) {
    $scope = $expires_items->getEntity()->scope->value;
    $now = \Drupal::time()->getCurrentTime();
    $expires = $expires_items->getValue();
    if ($scope) {
      if ($now > $expires) {
        $this->context
          ->buildViolation($constraint->dateEarly)
          ->addViolation();
      }
    }
    elseif ($now < $expires) {
      $this->context
        ->buildViolation($constraint->datelate)
        ->addViolation();
    }
  }

}

