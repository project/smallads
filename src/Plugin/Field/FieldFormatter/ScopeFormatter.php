<?php

namespace Drupal\smallads\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Field formatter.
 *
 * Shows an empty div with class according to whether the given date is before.
 * or after now.
 */
#[FieldFormatter(
  id: 'smallad_scope',
  label: new TranslatableMarkup('Visibility'),
  field_types: ['smallad_scope']
)]
class ScopeFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    return [[
      '#theme' => 'smallad_scope',
      '#scope' => $items->value,
    ]];
  }

}
