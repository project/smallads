<?php

namespace Drupal\smallads\Plugin\Field\FieldType;

use Drupal\smallads\Entity\SmalladInterface;
use Drupal\group\Entity\GroupRelationshipType;
use Drupal\Core\TypedData\OptionsProviderInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\IntegerItem;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Field\Attribute\FieldType;

/**
 * Defines the 'scope' smallad field type.
 */
#[FieldType(
  id: 'smallad_scope',
  label: new TranslatableMarkup('Smallad Scope'),
  description: new TranslatableMarkup('How widely this ad can be seen'),
  category: 'Smallads',
  default_widget: 'options_buttons'
)]
class ScopeItem extends IntegerItem implements OptionsProviderInterface{

  /**
   * {@inheritdoc}
   */
  public function getPossibleValues(AccountInterface $account = NULL) {
    return array_keys(static::validScopes());
  }

  /**
   * {@inheritdoc}
   */
  public function getPossibleOptions(AccountInterface $account = NULL) {
    return static::validScopes();
  }

  /**
   * {@inheritdoc}
   */
  public function getSettableValues(AccountInterface $account = NULL) {
    return $this->getPossibleValues($account);
  }

  /**
   * {@inheritdoc}
   */
  public function getSettableOptions(AccountInterface $account = NULL) {
    return $this->getPossibleOptions($account);
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    return rand($this->getPossibleValues());
  }

  /**
   *
   * Entity field default callback for scope field.
   * The default scope for a smallad is based on the settings for its owner.
   *
   * @param SmalladInterface $smallad
   *
   * @return int
   *
   * @todo add a setting for the site default.
   */
  public static function smalladsDefaultScope(SmalladInterface $smallad) : int {
    $scope = \Drupal::service('user.data')->get('smallads', $smallad->getOwnerId(), 'defaultscope') ?:
      SmalladInterface::SCOPE_SITE;
    return $scope;
  }

  /**
  * Get the list of scopes for this site.
  */
  static function validScopes() {
    $scopes[SmalladInterface::SCOPE_PRIVATE] = t('Hidden', [], ['context' => 'visibility scope']);
    if (\Drupal::moduleHandler()->moduleExists('smallads_group')) {
      if (GroupRelationshipType::loadByEntityTypeId('smallad')) {
        $scopes[SmalladInterface::SCOPE_GROUP] = t('Your groups', [], ['context' => 'visibility scope']);
      }
    }
    $scopes[SmalladInterface::SCOPE_SITE] = t('This site', [], ['context' => 'visibility scope']);
    if (\Drupal::moduleHandler()->moduleExists('smallads_murmurations')) {
      $scopes[SmalladInterface::SCOPE_NETWORK] = t('Partner network', [], ['context' => 'visibility scope']);
      if (\Drupal::config('smallads_murmurations.settings')->get('public')) {
        $scopes[SmalladInterface::SCOPE_PUBLIC] = t('Public', [], ['context' => 'visibility scope']);
      }
    }
    return $scopes;
  }

  /**
   * @param int $scope
   * @return TranslatableMarkup
   */
  static function getDescription(int $scope) : TranslatableMarkup {
    static $descriptions;
    if (!$descriptions) {
      $descriptions = [
        SmalladInterface::SCOPE_PRIVATE => t('Only you and admins can see this ad', [], ['context' => 'visibility scope']),
        SmalladInterface::SCOPE_GROUP => t('Publish in your groups', [], ['context' => 'visibility scope']),
        SmalladInterface::SCOPE_SITE => t('Visible to members of this site.', [], ['context' => 'visibility scope']),
        SmalladInterface::SCOPE_NETWORK => t('Visible to sites in our partner network.', [], ['context' => 'visibility scope']),
        SmalladInterface::SCOPE_PUBLIC => t('Visible to the public and to search engines.', [], ['context' => 'visibility scope'])
      ];
    }
    return $descriptions[$scope];
  }
}
