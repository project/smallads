<?php

namespace Drupal\smallads\Plugin\Block;

use Drupal\smallads\Entity\SmalladType;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Block\Attribute\Block;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Provides a block to start creating a smallad.
 */
#[Block(
  id: 'add_smallad',
  admin_label: new TranslatableMarkup('Add Smallad'),
  category: new TranslatableMarkup('Smallads')
)]
class AddSmallad extends BlockBase implements ContainerFactoryPluginInterface {

  const FORMCLASS = '\Drupal\smallads\Form\PreAddForm';
  /**
   * The form builder service.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, $form_builder) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->formBuilder = $form_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('form_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'adtype' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    foreach (SmalladType::loadMultiple() as $id => $adType) {
      $types[$id] = $adType->label();
    }
    $form['adtype'] = [
      '#title' => $this->t('Ad Type'),
      '#description' => $this->t('This should agree with the block title.'),
      '#type' => 'select',
      '#options' => $types,
      '#default_value' => $this->configuration['adtype'],
      '#empty_option' => $this->t('User selects type')
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    foreach ($form_state->getValues() as $key => $val) {
      $this->configuration[$key] = $val;
    }
  }

  /**
   * {@inheritdoc}
   *
   * @todo this result could be cached better.
   */
  protected function blockAccess(AccountInterface $account) {
    // Only grant access to users with the 'access news feeds' permission.
    return AccessResult::allowedIf(
      $account->hasPermission('post smallad') and
      $account->isAuthenticated() and
      \Drupal::routeMatch()->getRouteName() != 'entity.smallad.add_form'
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $ad_type = $this->configuration['adtype'];
    return $this->formBuilder->getForm(SELF::FORMCLASS, $ad_type);
  }

}

