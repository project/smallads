<?php

namespace Drupal\smallads;

use Drupal\Core\Entity\EntityViewBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Render controller for Smallads.
 */
class SmalladViewBuilder extends EntityViewBuilder {

  /**
   * {@inheritdoc}
   */
  protected function getBuildDefaults(EntityInterface $entity, $view_mode) {
    $build = parent::getBuildDefaults($entity, $view_mode);
    if ($view_mode != 'default' && $view_mode != 'full') {
      $build['#theme'] = 'smallad_' . $view_mode;
    }
    $build['#attached']['library'][] = 'smallads/css';
    return $build;
  }


  public function buildComponents(&$build, array $entities, array $displays, $view_mode) {
    parent::buildComponents($build, $entities, $displays, $view_mode);
    $current_user = \Drupal::currentUser();
    $display = reset($displays);
    foreach ($entities as $id => $smallad) {
      $build[$id]['links'] = [
        '#theme' => 'links',
        '#links' => []
      ];
      if ($info = $display->getComponent('links')) {
        $build[$id]['links']['#weight'] = $info['weight'];
        if ($current_user->hasPermission('access user contact forms')) {
          $build[$id]['links']['#links'][] = [
            'title' => t(
              "Contact %name",
              ['%name' => $smallad->getOwner()->label()]
            ),
            'url' => Url::fromRoute(
              'entity.user.contact_form',
              ['user' => $smallad->getOwnerId()],
              ['query' => ['subject' => $smallad->label()]]
            ),
          ];
        }
        if ($current_user->hasPermission('access user profiles')) {
          $build[$id]['links']['#links'][] = [
            'title' => t(
              "See @name's profile",
              ['@name' => $smallad->getOwner()->label()]
            ),
            'url' => Url::fromRoute(
              'entity.user.canonical',
              ['user' => $smallad->getOwnerId()]
            ),
          ];
        }
        if ($current_user->hasPermission('post smallad')) {
          $build[$id]['links']['#links'][] = [
            'title' => t(
              "See @name's @types",
              ['@name' => $smallad->getOwner()->label(), '@type' => $smallad->type->entity->label()]
            ),
            'url' => Url::fromRoute(
              // See config views.view.smallads_user.
              'view.smallads_user.my_ads',
              ['user' => $smallad->getOwnerId()]
            ),
          ];
        }
      }
      if ($display->getComponent('comment_count')) {
        $count = isset($build[$id]['comments']) ? count(Element::children($build[$id]['comments'])) : 0;
        $build[$id]['comment_count']['#markup'] = t('@count comments', ['@count' => $count]);
      }
      if ($current_user->isAnonymous()) {
        $build[$id]['links']['#links'][] = [
          'title' => t(
            "Sign up to @site",
            ['@site' => \Drupal::config('system.site')->get('name')]
          ),
          'url' => Url::fromRoute(
            'user.register',
          ),
        ];
      }

      $build[$id]['#attributes']['class'][] = 'smallad--type-' . $smallad->type->target_id;
      $build[$id]['#attributes']['class'][] = 'smallad--view-mode-' . $view_mode;
    }
    return $build;
  }

}
