<?php

/**
 * @file
 *
 * The module is designed to be ready to go and yet have some useful options
 * so there are many preset and dependencies on things like taxonomy, views and
 * menu items.
 * Each ad has a visibility scope and an expiry date. After the expiry date the
 * ad reverts to private scope where only the owner and permitted users can see
 * it.
 */

use Drupal\smallads\Entity\SmalladInterface;
use Drupal\smallads\Plugin\Field\FieldType\ScopeItem;
use Drupal\comment\Plugin\Field\FieldType\CommentItemInterface;
use Drupal\migrate\MigrateSkipRowException;
use Drupal\views\Entity\View;
use Drupal\Core\Url;
use Drupal\Core\Render\Element;
use Drupal\Core\Entity\Entity\EntityFormDisplay;

/**
 * Name of vocab created by this module.
 */
const SMALLAD_CATEGORIES_VID = 'categories';

/**
 * Implements hook_help().
 */
function smallads_help($route_name) {
  if ($route_name == 'admin.smallad') {
    $help[] = t('Ads are organised using two vocabularies.');
    $help[] = t('The first is like categories and subcategories in the yellow pages.');
    $help[] = t('Create and organise your categories here:');
    $help[] = '<br /><strong>' . l(t('Edit categories'), Url::fromRoute('entity.taxonomy_vocabulary.overview_form', ['taxonomy_vocabulary' => 'categories'])) . '</strong>';
    $help[] = '<p>' . t("'The optional second way to find things creates extra tabs on the listings pages.");
    $help[] = '<br /><strong>' . l(t('Edit types'), 'admin/structure/taxonomy/smallads_types') . '</strong></p>';
    $help[] = t('A third vocabluary, types, should be treated like a content type e.g. offers, wants, notices.');
    return implode(' ', $help);
  }
}

/**
 * Implements hook_ENTITY_TYPE_insert().
 *
 * Notify the owner of the ad that a comment has been added and reindex search.
 *
 * @todo this notifying should be done with rules module
 * @todo this should be a rule, preconfigured
 */
function smallads_comment_insert($comment) {
  if ($comment->getCommentedEntityTypeId() == 'smallad') {
    // On occaision this seems to be empty.
    if ($smallad = $comment->getCommentedEntity()) {
      \Drupal::service('plugin.manager.mail')->mail(
        'smallads',
        'comment',
        $smallad->getOwner()->getEmail(),
        $smallad->getOwner()->getPreferredLangcode(),
        [
          'comment' => $comment,
          'smallad' => $smallad,
          'user' => $smallad->getOwner(),
        ]
      );
    }
    smallads_reindex_smallad_search($comment->getCommentedEntityId());
  }
}

/**
 * Implements hook_comment_update().
 */
function smallads_comment_update($comment) {
  if ($comment->getCommentedEntityTypeId() == 'smallad') {
    smallads_reindex_smallad_search($comment->getCommentedEntityId());
  }
}

/**
 * Implements hook_comment_delete().
 */
function smallads_comment_delete($comment) {
  if ($comment->getCommentedEntityTypeId() == 'smallad') {
    smallads_reindex_smallad_search($comment->getCommentedEntityId());
  }
}

/**
 * Marks a Smallad to be re-indexed.
 * @param int $smid
 * @todo why isn't this called by the Smallad Entity Controller?
 */
function smallads_reindex_smallad_search(int $smid) {
  if (\Drupal::moduleHandler()->moduleExists('search')) {
    \Drupal::service('search.index')->markForReindex('smallad_search', $smid);
  }
}

/**
 * Implements hook_theme().
 */
function smallads_theme() {
  $items['smallad'] = [
    'render element' => 'elements',
  ];
  $items['smallad_listing'] = [
    'render element' => 'elements',
  ];
  $items['smallad_search_index'] = [
    'render element' => 'elements',
  ];
  $items['smallad_scope_widget'] = [
    'render element' => 'elements',
  ];
  $items['smallad_scope'] = [
    'variables' => [
      'scope' => 0,
      'smallad' => NULL
    ]
  ];
  return $items;
}

/**
 * Theme preprocessing hook
 * The scope widget presents a series of large buttons.
 */
function template_preprocess_smallad_scope_widget(&$vars) {
  $children = Element::children($vars['elements']['widget']);
  foreach ($children as $key) {
    $element = $vars['elements']['widget'][$key];
    $width_percent = 100/count($children) - 7;
    $image = ['#theme' => 'smallad_scope', '#scope' => $element['#return_value']];
    $vars['options'][$element['#id']] = [
      'label' => $element['#title'],
      'id' => $element['#id'],
      'value' => $element['#return_value'],
      'checked' => $element['#return_value'] == $vars['elements']['widget']['#default_value'],
      'width' => (int)$width_percent . '%',
      'icon' => \Drupal::service('renderer')->render($image)
    ];
  }
}

/**
 * Default theme preprocessor for 'smallad'.
 */
function template_preprocess_smallad(&$vars) {
  $build = &$vars['elements'];
  // Put all the fields into content, like the node.
  foreach (Element::Children($build) as $key) {
    $vars['content'][$key] = $build[$key];
  }
}

/**
 * Default theme preprocessor for 'smallad'.
 */
function template_preprocess_smallad_scope(&$vars) {
  $vars['chars'] = 3;
  $scopes = ScopeItem::validScopes();
  $vars['title'] = $scopes[$vars['scope']];
  $vars['description'] = ScopeItem::getDescription($vars['scope']);
  foreach (ScopeItem::validScopes() as $name) {
    $vars['chars'] = max(strlen($name), $vars['chars']);
  }
}

/**
 * Default theme preprocessor for 'smallad_listing'.
 */
function template_preprocess_smallad_listing(&$vars) {
  // Only show the first image.
  foreach (Element::children($vars['elements']['image']) as $delta) {
    if ($delta) {
      unset($vars['elements']['image'][$delta]);
    }
  }
  template_preprocess_smallad($vars, FALSE);
  $vars['more'] = $vars['elements']['#smallad']->toLink(t('More...'));
  $vars['title'] = $vars['elements']['#smallad']->title->value;
}

/**
 * Implements hook_cron().
 *
 * Manage proposition expiry and notifications by checking for ads past their
 * expiry date. The act of unpublishing is queued because it involves sending an
 * email.
 */
function smallads_cron() {
  // Automatic expiry of nodes, notifies the owner.
  $now = gmdate('Y-m-d', \Drupal::time()->getRequestTime());
  $smids = \Drupal::entityQuery('smallad')->accessCheck(TRUE)
    // Actually the date is stored as dd-mm-yyyy.
    ->condition('expires', $now, '<')
    ->condition('scope', 0, '>')
    ->execute();
  if ($smids) {
    $queue = \Drupal::queue('smallads_expired_mail');
    foreach ($smids as $smid) {
      $queue->createItem($smid);
    }
  }
}

/**
 * Implements hook_mail().
 */
function smallads_mail($key, &$message, $params) {
  if ($key == 'expired') {
    $mail_template = \Drupal::Config('smallads.settings')->get('expiry_mail');
    $message['subject'] = \Drupal::Token()->replace($mail_template['subject'], $params);
    $message['body'][] = \Drupal::Token()->replace($mail_template['body'], $params);
  }
  elseif ($key == 'comment') {
    $mail_template = \Drupal::Config('smallads.settings')->get('comment_mail');
    $message['subject'] = \Drupal::Token()->replace($mail_template['subject'], $params);
    //You received a comment from [user:display-name]
    $message['body'][] = \Drupal::Token()->replace($mail_template['body'], $params);
  }
}

/**
 * Implements hook_locale_translation_projects_alter().
 * Load translations from Cforge server
 */
function smallads_locale_translation_projects_alter(&$projects) {
  if (\Drupal::languageManager()->getDefaultLanguage()->getId() == 'fr') {
    $projects['smallads']['server_pattern'] = 'http://translations.communityforge.net/%core/%project.%language.po';
  }
}

/**
 * Sometimes need to know whether the context indicates an ad-type on a route.
 */
function smallad_type_from_route_match() {
  $route_params = \Drupal::service('current_route_match')->getParameters();
  if ($route_params->has('smallad')) {
    return $route_params->get('smallad')->id();
  }
  // Doesn't seem very likely.
  elseif ($route_params->has('view_id')) {
    return $route_params->get('arg_0');
  }
  elseif ($route_params->has('smallad_type')) {
    // Or could find it in the raw parameters.
    return $route_params->get('smallad_type')->id();
  }
}

/**
 * Implements hook_entity_extra_field_info().
 */
function smallads_entity_extra_field_info() {
  foreach (\Drupal::entityTypeManager()->getStorage('smallad_type')->loadMultiple() as $bundle_name => $type) {
    $fields['smallad'][$bundle_name] = [
      'display' => [
        'links' => [
          'label' => t('Links'),
          'description' => t("Links to related ads, duplicates categories field"),
          'weight' => 6,
        ],
        'comment_count' => [
          'label' => t('Comment count'),
          'description' => t("The number of comments on the article"),
          'weight' => 8,
        ],
      ],
    ];
  }
  return $fields;
}

/**
 * Implements hook_views_query_alter().
 * Filter smallads by a scope appropriate to user access.
 */
function smallads_views_query_alter($view, $query) {
  $tables = $view->getBaseTables();
  if (isset($tables['smallads_field_data'])) {
    // This min scope would need to be expanded to support groups.
    $min_scope = (\Drupal::currentUser()->id() > 0) ? 1 : 3;
    $query->where[] = [
      'conditions' => [
        [
          'field' => 'smallad_field_data.scope',
          'value' => $min_scope,
          'operator' => '>'
        ],
        [
          'field' => 'smallad_field_data.uid',
          'value' => \Drupal::currentUser()->id(),
          'operator' => '='
        ]
      ],
      'args' => [],
      'type' => 'OR'
    ];
  }
}

/**
 * Implements hook_migration_plugins_alter().
 *
 * Change the default Field API migration process
 */
function smallads_migration_plugins_alter(array &$definitions) {
  // Map permissions
  $definitions['d7_user_role']['process']['permissions'][0]['map'] += [
    'post proposition' => 'post smallad',
    'edit propositions'  => 'edit all smallads',
  ];
  //Don't migrate the node type config entity.
  unset($definitions['d7_node:proposition']);
  unset($definitions['d7_node_complete:proposition']);
  unset($definitions['d7_node_revision:proposition']);
  unset($definitions['d7_taxonomy_term:offers_wants_types']);
  // This is needed when the vocab is hierarchical.
  $definitions['d7_taxonomy_term:offers_wants_categories']['process']['parent_id'][1]['migration'] = 'd7_taxonomy_term:offers_wants_categories';
  //in 8.4-rc2 menu items are failing validation because their routes haven't been migrated yet
  $definitions['d7_menu_links']['migration_dependencies']['required'][] = 'd7_smallad';

  $definitions['d7_block']['process']['plugin'][0]['map']['offers_wants']['add_proposition'] = 'add_smallad';
  $definitions['d7_block']['process']['plugin'][0]['map']['offers_wants']['propositions_taxonomy_block'] = 'views_block:smallads_admin-categories';
  $definitions['d7_field_instance_widget_settings']['process']['options/type']['type']['map']['taxonomy_shs'] = 'options_shs';
}

/**
 * Implements hook_config_translation_info().
 */
function smallads_config_translation_info(&$info) {
  $info['smallad_type'] = [
    'class' => '\Drupal\config_translation\ConfigEntityMapper',
    'base_route_name' => 'entity.smallad_type.edit_form',
    'title' => t('Smallad type'),
    'entity_type' => 'smallad_type',
  ];
}

/**
 * Implements hook_entity_display_build_alter().
 *
 * Rewrite the category link to show taxonomy term listings for the presented
 * bundle. Could not use smallads_entity_bundle_info_alter() because
 * EntityReferenceLabelFormatter in D8.6 for both EntityViewDisplay and Views
 * fields only references the canonical link.
 */
function smallads_entity_display_build_alter(&$build, $context) {
  // Context = entity, view_mode, display
  if ($context['entity']->getEntityTypeId() == 'smallad' && isset($build['categories'])) {
    $path = '/'.$context['entity']->bundle() .'s';
    foreach (Element::children($build['categories']) as $key) {
      $renderable = &$build['categories'][$key];
      if (isset($renderable['#type']) and $renderable['#type'] == 'link') {
        $tid  = $renderable['#options']['entity']->id();
        $renderable['#url'] =  Url::fromUserInput($path, ['query' => ['adcats' => $tid]]);
      }
    }
  }
}

/**
 * Implements hook_field_widget_info_alter();
 */
function smallads_field_widget_info_alter(array &$definitions) {
  $definitions['options_select']['field_types'][] = 'smallad_scope';
  $definitions['options_buttons']['field_types'][] = 'smallad_scope';
}

/**
 * Implements hook_migrate_prepare_row().
 */
function smallads_migrate_d7_menu_links_prepare_row($row, $source, $migration) {
  // Only migrate menu links which are not already in the menu tree
  $title = $row->getSourceProperty('link_title');
  if (in_array($title, array((string)t('Offer'), (string)t('Want')))) {
    throw new MigrateSkipRowException("Menu link already installed: ".$title);
  }
}

/**
 * Implements hook_ENTITY_TYPE_delete().
 * Delete all the user's smallads.
 */
function smallads_user_delete($entity) {
  $ads = \Drupal::entityTypeManager()
    ->getStorage('smallad')
    ->loadByProperties(['uid' => $entity->id()]);
  if ($ads) {
    \Drupal::messenger(t('Deleted @count Small Ads', ['@count' => count($ads)]));
    foreach ($ads as $ad) {
      $ad->delete();
    }
  }
}

/**
 * Implements hook_form_alter().
 * When choosing a views handler, some ad blockers may hide smallads
 */
function smallads_form_views_ui_add_handler_form_alter(&$form, $form_state) {
  // removing the smallad class seems to change the rows from visible to invisible. More testing needed.
  foreach ($form['options']['name']['#options'] as &$opt) {
    if($pos = array_search('smallad', $opt['#attributes']['class'])) {
      //unset($opt['#attributes']['class'][$pos]);
    }
  }
  if (isset($form['override']['controls']['group']['#options']['smallad'])) {
    $form['options']['name']['#caption'] = t('N.B. It may be necessary to disable your ad-blocker to see the smallad fields.');
  }
}

/**
 * Implements hook_ENTITY_TYPE_insert().
 * Add a display for each smallad type to each of the stub views.
 */
function smallads_smallad_type_insert($smallad_type) {
  if (!in_array($smallad_type->id(), ['offer', 'want'])) {
    /** @var Drupal\smallads\Entity\SmalladType $smallad_type */
    $views_service = \Drupal::service('views.executable');
    \Drupal::moduleHandler()->loadInclude('smallads', 'install');
    foreach (View::loadMultiple() as $id => $view) {
      if (substr($id, 0, 14) == 'smallads_auto_') {
        $parts = explode('_', $id);
        // Clone the default display and set the smallad_type
        $executable = $views_service->get($view);
        smallads_new_display($executable, end($parts), $smallad_type);
        $view->save();
      }
    }
  }
}

/**
 * Implements hook_ENTITY_TYPE_delete();
 */
function smallads_smallad_type_delete($smallad_type) {
  /** @var Drupal\smallads\Entity\SmalladType $smallad_type */
  $type_label = $smallad_type->label();
  $views_service = \Drupal::service('views.executable');
  foreach (View::loadMultiple() as $id => $view) {
    if (substr($id, 0, 14) == 'smallads_auto_') {
      $parts = explode('_', $id);
      $display_type = end($parts);
      $displays = $view->get('display');
      $displays[$smallad_type->id().'_'.$display_type]['deleted'] = TRUE;
      $view->set('display', $displays);
      $view->save();
    }
  }
}

/**
 * Implements hook_view_data_alter()
 */
function smallads_views_data_alter(&$data) {
  $data['smallad']['smallad_shs_taxonomy_entity_index_tid_depth']['group'] = 'Smallad';
  $data['smallad']['smallad_shs_taxonomy_entity_index_tid_depth']['filter']['id'] = 'smallad_shs_taxonomy_entity_index_tid_depth';
  $data['smallad']['smallad_shs_taxonomy_entity_index_tid_depth']['filter']['title'] = t('Categories');
}

/**
 * Implements hook_module_implements_alter().
 * Ensure that taxonomy_entity_index_views_data_alter runs before smallads_views_data_alter
 */
function smallads_module_implements_alter(&$implementations, $hook) {
  if ($hook == 'views_data_alter') {
    unset($implementations['smallads']);
    $implementations['smallads'] = 'views';
  }
}

/**
 * Implements hook_ENTITY_TYPE_update().
 */
function smallads_taxonomy_term_update($term) {
  if ($term->vid->target_id == 'categories') {
    $hierarchy_on = \Drupal::moduleHandler()->moduleExists('shs_chosen') ;
    $hierarchy_needed = smallads_has_hierarchical_term();
    if ($hierarchy_on <> $hierarchy_needed) {
      smallads_set_category_hierarchy($hierarchy_needed);
    }
  }
}

/**
 * Implements hook_ENTITY_TYPE_insert().
 */
function smallads_taxonomy_term_insert($term) {
  smallads_taxonomy_term_update($term);
}

/**
 * Find out if any of the category terms has a parent;
 * @return bool
 */
function smallads_has_hierarchical_term() : bool {
  $all_terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadByProperties(['vid' => 'categories']);
  foreach ($all_terms as $cat_term) {
    if ($cat_term->parent->target_id) {
      return TRUE;
    }
  }
  return FALSE;
}

/**
 * Utility
 * Change the taxonomy widget according to whether the categories vocab is hierarchical.
 * @param boolean $mode
 *   TRUE if the categories vocab is hierarchical.
 */
function smallads_set_category_hierarchy(bool $mode) {
  if ($mode) {
    \Drupal::service('module_installer')->install(['shs_chosen']);
  }
  /** @var EntityFormDisplay $form_display */
  foreach (EntityFormDisplay::loadMultiple() as $form_display) {
    if ($settings = $form_display->getComponent('categories')) {
      $settings['type'] = $mode ? 'options_shs_chosen' : 'chosen_select';
      $form_display->setComponent('categories', $settings)->save();
    }
  }
  if (!$mode) {
    \Drupal::service('module_installer')->uninstall(['shs_chosen']);
  }
  \Drupal::messenger()->addStatus('Smallads hierarchy mode: '.($mode ? 'On' : 'Off'));
}

/**
 * Implements hook_ENTITY_TYPE_presave().
 * Ensure comments are always OPEN.
 * And that the user's contact form is public if they have a public offer.
 */
function smallads_smallad_presave($smallad) {
  if (isset($smallad->comments)) {
    $smallad->comments->status = CommentItemInterface::OPEN; //2
  }
  if ($smallad->scope->value > SmalladInterface::SCOPE_SITE ) {
    $userdata = \Drupal::service('user.data');
    $setting = $userdata->get('contact', $smallad->getOwnerId(), 'enabled');
    if (!$setting) {
      $userdata->set('contact', $smallad->getOwnerId(), 'enabled', 1);
      \Drupal::messenger()->addStatus(t('Your contact settings have been adjusted so that interested parties will be able to contact you through this site.'));
    }
  }
}

/**
 * Implements hook_preprocess_THEME_HOOK().
 */
function smallads_preprocess_field__categories(&$vars) {
  foreach ($vars['items'] as &$item) {
    if ($desc = $item['content']['#entity']->description->value) {
      $item['content']['#attributes']['title'] = $item['content']['#entity']->description->value;
    }
  }
}
