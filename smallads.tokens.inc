<?php

/**
 * @file
 * Token hooks.
 * @deprecated, or at least unnecessary now that the token module is required.
 */

use Drupal\smallads\Entity\SmalladType;
use Drupal\Core\Language\LanguageInterface;

/**
 * Implements hook_tokens().
 */
function smallads_tokens($type, $tokens, array $data = [], array $options = []) {

  if (!array_key_exists($type, $data) || !is_object($data[$type])) {
    return;
  }

  $url_options = ['absolute' => TRUE];
  if (isset($options['langcode'])) {
    $url_options['language'] = \Drupal::languageManager()->getLanguage($options['langcode']);
    $langcode = $options['langcode'];
  }
  else {
    $langcode = LanguageInterface::LANGCODE_DEFAULT;
  }

  $replacements = [];
  if ($type == 'smallad') {
    $smallad = $data['smallad'];
    /** @var \Drupal\smallads\Entity\Smallad $smallad */
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'type':
          $replacements[$original] =$smallad->bundle();
          break;
        case 'type-name':
          $replacements[$original] = SmalladType::load($smallad->bundle())->label();
          break;
        case 'smid':
          $replacements[$original] = $smallad->id();
          break;
        case 'title':
          $replacements[$original] = $smallad->label();
          break;
        case 'body':
          $replacements[$original] = $smallad->body->value;
          break;
        case 'created':
          $replacements[$original] = \Drupal::service('date.formatter')->format($smallad->getCreatedTime(), 'medium', '', NULL, $langcode);
          break;
        case 'expires':
          $replacements[$original] = \Drupal::service('date.formatter')->format(strtotime($smallad->expires->value), 'medium', '', NULL, $langcode);
          break;
        case 'url':
          $replacements[$original] = $smallad->toUrl('canonical', $url_options)->toString();
          break;
        case 'edit-url':
          $replacements[$original] = $smallad->toUrl('edit-form', $url_options)->toString();
          break;
        case 'link':
          $replacements[$original] = $smallad->toLink()->toString();
          break;
      }
    }
  }
  return $replacements;
}

function smallads_token_info() {
  $type = [
    'name' => t('Smallads'),
    'description' => t('Tokens related to individual small ads.'),
    'needs-data' => 'smallad',
  ];

  // Core tokens.
  $smallad['smid'] = [
    'name' => t("Small ad ID"),
    'description' => t('The unique ID of the ad.'),
  ];
  $smallad['type'] = [
    'name' => t("The bundle name of the smallad type"),
  ];
  $smallad['type-name'] = [
    'name' => t("The name of the smallad type"),
  ];
  $smallad['title'] = [
    'name' => t("One-line description"),
  ];

  $smallad['body'] = [
    'name' => t("Body"),
    'description' => t("Tell the story behind this ad."),
  ];
  $smallad['owner'] = [
    'name' => t('Posted by'),
    'type' => 'user',
    'description' => t("The owner of the small ad."),
  ];
  $smallad['image'] = [
    'name' => t("Image"),
    'type' => 'file',
  ];

  // Chained tokens for nodes.
  $smallad['created'] = [
    'name' => t("Authored by"),
    'type' => 'date',
  ];
  $smallad['changed'] = [
    'name' => t("Last changed"),
    'type' => 'date',
  ];
  // Chained tokens for nodes.
  $smallad['expires'] = [
    'name' => t("Expiry date"),
    'type' => 'date',
  ];
  $smallad['url'] = [
    'name' => t("URL"),
    'description' => t("The absolute URL of the ad."),
  ];
  $smallad['edit-url'] = [
    'name' => t("Edit URL"),
    'description' => t("The URL of the ad's edit page."),
  ];

  return [
    'types' => ['smallad' => $type],
    'tokens' => ['smallad' => $smallad],
  ];
}
